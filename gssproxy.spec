%global gpstatedir %{_localstatedir}/lib/gssproxy

Summary:		A proxy for GSSAPI
Name:			gssproxy
Version:		0.9.1
Release:		7%{?dist}
License:		MIT
URL:			https://github.com/gssapi/gssproxy
Source0:		%{url}/releases/download/v%{version}/%{name}-%{version}.tar.gz
Source1:        gssproxy.sock.compat.conf

BuildRequires:	autoconf automake make m4 git pkgconfig libtool
BuildRequires:	docbook-style-xsl doxygen findutils
BuildRequires:	gettext-devel keyutils-libs-devel libselinux-devel
BuildRequires:	libverto-devel libxml2 libxslt popt-devel systemd-devel systemd-units
BuildRequires:	krb5-devel >= 1.12.0
BuildRequires:	libini_config-devel >= 1.2.0
Requires:		keyutils-libs libverto-module-base
Requires: 		krb5-libs >= 1.12.0
Requires: 		libini_config >= 1.2.0
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
The GSSAPI (Generic Security Services API) is a RFC-standardized interface 
for applications that wish to use security libraries. gssproxy provides a 
daemon to manage access to GSSAPI credentials. gssproxy consists of the 
gssproxy daemon (configured by the gssproxy.conf(5) file) and a GSSAPI 
interposer plugin (gssproxy-mech(8)).

%prep
%autosetup

%build
autoreconf -fiv
%configure \
    --with-pubconf-path=%{_sysconfdir}/gssproxy \
    --with-socket-name=%{_rundir}/gssproxy.default.sock \
    --with-initscript=systemd \
    --disable-static \
    --disable-rpath \
    --with-gpp-default-behavior=REMOTE_FIRST

%make_build

%install
%make_install

mkdir -p %{buildroot}%{_sysconfdir}/gssproxy
install -m 0644 examples/gssproxy.conf %{buildroot}%{_sysconfdir}/gssproxy/gssproxy.conf
install -m 0644 examples/99-network-fs-clients.conf %{buildroot}%{_sysconfdir}/gssproxy/99-network-fs-clients.conf

mkdir -p %{buildroot}%{_tmpfilesdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_tmpfilesdir}/gssproxy.conf

mkdir -p %{buildroot}%{gpstatedir}/rcache
ln -s %{_rundir}/gssproxy.default.sock %{buildroot}%{gpstatedir}/default.sock

rm -f %{buildroot}%{_libdir}/gssproxy/proxymech.la

%check
make test_proxymech

%pre
if [ -S %{gpstatedir}/default.sock ]; then
    rm -f %{gpstatedir}/default.sock
fi

%post
%systemd_post gssproxy.service

%preun
%systemd_preun gssproxy.service

%postun
%systemd_postun_with_restart gssproxy.service

%files
%license COPYING
%{_unitdir}/gssproxy.service
%{_userunitdir}/gssuserproxy.service
%{_userunitdir}/gssuserproxy.socket
%attr(755,root,root) %dir %{gpstatedir}
%attr(700,root,root) %dir %{gpstatedir}/clients
%attr(700,root,root) %dir %{gpstatedir}/rcache
%{gpstatedir}/default.sock
%attr(755,root,root) %dir %{_sysconfdir}/gssproxy
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/gssproxy/gssproxy.conf
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/gssproxy/99-network-fs-clients.conf
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/gss/mech.d/proxymech.conf
%{_tmpfilesdir}/%{name}.conf
%{_sbindir}/gssproxy
%dir %{_libdir}/gssproxy
%{_libdir}/gssproxy/proxymech.so
%{_mandir}/man5/gssproxy.conf.5*
%{_mandir}/man8/gssproxy.8*
%{_mandir}/man8/gssproxy-mech.8*

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.1-7
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Thu Sep 19 2024 Tao Wu <tallwu@tencent.com> - 0.9.1-6
- [Type] bugfix
- [DESC] Add systemd-devel on BuildRequires

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.1-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.1-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.1-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.1-2
- Rebuilt for OpenCloudOS Stream 23

* Fri Jul 22 2022 Xiaojie Chen <jackxjchen@tencent.com> - 0.9.1-1
- Initial build
